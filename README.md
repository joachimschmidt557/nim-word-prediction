# nim-word-prediction

A very simple and naive nim program which predicts next words

**Warning:** `nim-word-prediction` uses naive methods using matrices
and therefore can take up a huge amount of RAM. Be careful.

## Building

```
$ git clone https://gitlab.com/joachimschmidt557/nim-word-prediction
$ cd nim-word-prediction
$ nimble build
```

## Usage examples

Predict the next word to `because` using a text stored in `data.txt`:

```
$ ./nim_word_prediction --text=data.txt because
```

Interactive mode - predict words from stdin:

```
$ ./nim_word_prediction --text=data.txt -i
```

Print simple statistics (i.e. size of vocabulary)

```
$ ./nim_word_prediction --text=data.txt -i -s
```
