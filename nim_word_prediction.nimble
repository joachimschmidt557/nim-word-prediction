# Package

version       = "0.1.0"
author        = "joachimschmidt557"
description   = "Next word prediction"
license       = "MIT"
srcDir        = "src"
bin           = @["nim_word_prediction"]



# Dependencies

requires "nim >= 0.20.0"
requires "neo >= 0.2.5"
