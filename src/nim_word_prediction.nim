import parseopt, tables, strutils

import neo

proc predict(word:string, matrix:Matrix[uint16],
             word2int:TableRef[string, int],
             int2word:TableRef[int, string]):string =
  if not word2int.hasKey(word):
    return ""
  let
    w = word2int[word]
  var
    maxI = 0
    maxW = 0u16

  # Get the maximum of that row
  for i in 0 .. matrix.N - 1:
    let x = matrix[w, i]
    if x > maxW:
      maxI = i
      maxW = x
  return int2word[maxI]

proc updateMatrix(matrix:var Matrix[uint16], word1:string, word2:string,
                  word2int:var TableRef[string, int],
                  int2word:var TableRef[int, string]) =
  # Index the words if the aren't in the index yet
  if not word2int.hasKey(word1):
    let newIndex = len(word2int)
    word2int[word1] = newIndex
    int2word[newIndex] = word1
  if not word2int.hasKey(word2):
    let newIndex = len(word2int)
    word2int[word2] = newIndex
    int2word[newIndex] = word2

  # Compute the indices
  let
    w1 = word2int[word1]
    w2 = word2int[word2]

  # Check if the matrix is large enough
  if w1 < matrix.ld and w2 < matrix.ld:
    matrix[w1, w2] += 1

proc interactive(matrix:Matrix[uint16],
                 word2int:TableRef[string, int],
                 int2word:TableRef[int, string]) =
  echo "-= Interactive prediction =-"
  for line in stdin.lines:
    echo predict(line, matrix, word2int, int2word)

proc main() =
  var
    p = initOptParser()
    interactive = false
    statistics = false
    word = "I"
    texts:seq[string]
    matrix = zeros(20000, 20000, uint16, rowMajor)
    word2int = newTable[string, int]()
    int2word = newTable[int, string]()

  # Parse arguments
  while true:
    p.next()
    case p.kind
    of cmdEnd: break
    of cmdShortOption, cmdLongOption:
      if p.key == "i" or p.key == "interactive":
        interactive = true
      if p.key == "s" or p.key == "statistics":
        statistics = true
      if p.key == "t" or p.key == "text":
        texts.add(readFile(p.val))
    of cmdArgument:
      word = p.key

  # Generate matrix
  for text in texts:
    var prevWord = ""
    for word in text.split():
      updateMatrix(matrix, prevWord, word, word2int, int2word)
      prevWord = word

  if statistics:
    echo "-= Statistics =-"
    echo "Size of vocabulary: " & $word2int.len

  if interactive:
    interactive(matrix, word2int, int2word)
  else:
    echo predict(word, matrix, word2int, int2word)

when isMainModule:
  main()
